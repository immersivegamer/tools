﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Nager.Date;

namespace date_dimension_table
{
    class Program
    {
        static void Main(string[] argsString)
        {
            // inspired by:
            // http://web.archive.org/web/20070611150639/http://sqlserver2000.databases.aspfaq.com/why-should-i-consider-using-an-auxiliary-calendar-table.html
            
            int[] args = argsString.Select(x => int.Parse(x)).ToArray();

            using (var writer = new StreamWriter("./dates.csv"))
            using (var csv = new CsvWriter(writer))
            {    
                csv.WriteRecords(CalculateDates(args[0],args[1],args[2],args[3],args[4],args[5]));
            }
        }

        static IEnumerable<daterow> CalculateDates(int sy, int sm, int sd, int ey, int em, int ed)
        {
            var startdate = new DateTime(sy, sm, sd);
            var enddate = new DateTime(ey, em, ed);

            while (startdate < enddate)
            {
                yield return ConvertToDateRow(startdate);
                startdate = startdate.AddDays(1);
            }
        }

        static daterow ConvertToDateRow(DateTime dateTime)
        {
            return new daterow() {
                date = (dateTime.Year * 10000) + (dateTime.Month * 100) + dateTime.Day,
                isWeekday = dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday ? 0 : 1,
                isHoliday = DateSystem.IsPublicHoliday(dateTime, CountryCode.US) ? 1 : 0,
                year = dateTime.Year,
                month = dateTime.Month,
                day = dateTime.Day,
                weekday =(int) dateTime.DayOfWeek,
                monthname = dateTime.ToString("MMMM"),
                dayname = dateTime.ToString("dddd")
            };
        }
    }
}
