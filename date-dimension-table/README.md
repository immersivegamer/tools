## Details

### About

Tool to create a CSV file with a list of dates (as int) to be loaded into a SQL database. A table created with this data can be useful for doing date calculations. Program is fast for doing several centuries.

### Language

C#, .Net Core (Console) Project

### Dependencies

|Name|Link|About|
|----|----|-----|
|CsvHelper|https://joshclose.github.io/CsvHelper/|Used to write csv file.|
|Nager.Date|https://github.com/tinohager/Nager.Date|Used to calculate if a date is a holiday.|

## Usage

Build project.

```powershell
# navigate to project root
dotnet build -c release

# default output to: ./bin/release/netcoreapp2.2/
```

Run the program. You will need to supply the date range.

```powershell
# navigate to build folder
# from year, month, day - to year month day (space separating each value)
dotnet date-dimension-table.dll YYYY MM DD YYYY MM DD

# output at: ./dates.csv
```

```powershell
# example: 1950/01/01 to 2100/12/31
dotnet date-dimension-table.dll 1950 01 01 2100 12 31
```

## Example output

| date     | isWeekday | isHoliday | year | month | day | weekday | monthname | dayname   |
|----------|-----------|-----------|------|-------|-----|---------|-----------|-----------|
| 19500101 | 0         | 0         | 1950 | 1     | 1   | 0       | January   | Sunday    |
| 19500102 | 1         | 1         | 1950 | 1     | 2   | 1       | January   | Monday    |
| 19500103 | 1         | 0         | 1950 | 1     | 3   | 2       | January   | Tuesday   |
| 19500104 | 1         | 0         | 1950 | 1     | 4   | 3       | January   | Wednesday |
| ...      |
| 21001227 | 1         | 0         | 2100 | 12    | 27  | 1       | December  | Monday    |
| 21001228 | 1         | 0         | 2100 | 12    | 28  | 2       | December  | Tuesday   |
| 21001229 | 1         | 0         | 2100 | 12    | 29  | 3       | December  | Wednesday |
| 21001230 | 1         | 0         | 2100 | 12    | 30  | 4       | December  | Thursday  |

## How to load the data

You could write a script to convert data to basic SQL insert statements. Most SQL engines have a bulk upload. Or you can use DBeaver to connect to the CSV as a table and do an export table from the data into the desired location.