public class daterow {
    public int date { get; set; }
    public int isWeekday { get; set; }
    public int isHoliday { get; set; }
    public int year { get; set; }
    public int month { get; set; }
    public int day { get; set; }
    public int weekday { get; set; }
    public string monthname { get; set; }
    public string dayname { get; set; }    
}