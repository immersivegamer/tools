## List of Projects

| Name                 | Lang | Desc                                                                   |
|----------------------|------|------------------------------------------------------------------------|
| date-dimension-table | C#   | Create a CSV file to load into a SQL database to do date calculations. |
| csv-to-xml           | C#   | Convert a CSV flat file to a nested XML file                           |
| csv-to-input         | C#   | Input automation tool, for example filling in forms from data          |
| daeomon-template     | C# / .Net Core  | Template for creating a linux deamon with dotnet core       |
| mysql-restort        | PowerShell | Automate the restoration of mysql dumps, including running pre/post SQL scripts |
| xml-distinct         | C#   | Process a bunch of similar XML files to get all possible nodes         |