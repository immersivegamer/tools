namespace csv_to_input
{
    public class Config
    {
        public int delay { get; set; }
        public string csvFile { get; set; }
        public string[] actions { get; set; }
    }
}
