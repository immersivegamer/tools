1. You may need to download .Net Framework 4.6.1 from here and install it: https://www.microsoft.com/en-us/download/details.aspx?id=49981

2. Ensure you have a 'config.json' file is set up. Open with Notepad (or you can download VS Code which can help edit JSON files). See actions below.

3. Ensure you have a 'data.csv' file that matches your config file.

4. Double click to launch the EXE/BAT program.

5. Click in the text input/area that is the starting point. 

6. Use short cut Control-Insert to start. You must release the keys. One beep will sound when waiting for start.

7. Use short cut Insert to continue if you it a waiting point. If you reach the end of a record then the next record will be waiting to start (step 6). Two beeps will sound when waiting to continue.

8. A file of 'log.txt' will be created which records the date, time, what file was used as a CSV and what row was completed.

9. While the program is running the window that pops up will show what the next shortcut to press is. When all records are read the windows should automatically close.


List of Actions:
----------------

You can list these actions in any order and repeat as many times as you want in the config.json file.

"input" : Outputs text where the cursor currently is.
"tab"   : Peforms a Tab as if the TAB key was pressed.
"enter" : Performs a Return as if the RETURN/ENTER key was pressed.
"wait"  : Waits until the continue shortcut is pressed (INSERT key).


Example Config File:
--------------------

You can copy everything between and including the '{' and '}'. This is JSON format. Search online for more information. Key is that any '\' in text must become '\\' as '\' is a special charcter in JSON.

{
    "csvFile": "./data.csv",
    "actions" : [
        "insert",
        "tab", 
        "wait",
        "enter"
    ]
}