﻿using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using WindowsInput;
using WindowsInput.Native;

namespace csv_to_input
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var configData = System.IO.File.ReadAllText("./config.json");
                Config config = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(configData);
                string csvFilePath = config.csvFile;
                string[] actions = config.actions;

                var lines = GetLines(csvFilePath);
                var auto = new AutoInput(lines, actions, config.delay);
                auto.EnterWaitingForStart = () =>
                {
                    System.Console.WriteLine("Waiting for user to start (shortcut: control-insert)");
                    System.Console.Beep();
                };
                auto.EnterWaitingForContinue = () =>
                {
                    System.Console.WriteLine("Waiting for user to continue (shortcut: insert)");
                    System.Console.Beep();
                    System.Console.Beep();
                };
                auto.EndOfLine = (int recordNumber) =>
                {
                    System.Console.WriteLine($"Reached end of record data for line [{recordNumber}] or configured actions, moving to next record.");
                    Log(csvFilePath, recordNumber);
                };
                auto.Start();
            }
            catch (Exception ex)
            {
                Error(ex.ToString());
                throw;
            }
        }

        private static List<string[]> GetLines(string csvFilePath)
        {
            var list = new List<string[]>();
            using (var reader = new StreamReader(csvFilePath))
            {
                var parser = new CsvParser(reader);
                parser.Read(); // discard header row
                while (true)
                {
                    string[] row = parser.Read();

                    if (row == null)
                    {
                        break;
                    }

                    list.Add(row);
                }
            }
            return list;
        }

        private static void Log(string csvFileName, int recordNumber)
        {
            string logPath = "./log.txt";
            if (!File.Exists(logPath))
            {
                File.Create(logPath).Close();
            }

            File.AppendAllText(logPath, $"\n{DateTime.Now} - File: {csvFileName}; Completed Record [{recordNumber}]");
        }

        private static void Error(string errorMessage)
        {
            string errorPath = "./error.txt";
            if (!File.Exists(errorPath))
            {
                File.Create(errorPath).Close();
            }

            File.AppendAllText(errorPath, $"\n\n{DateTime.Now} - ERROR: {errorMessage}\n");
        }
    }
}
