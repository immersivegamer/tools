using System;
using System.Collections.Generic;
using System.Linq;
using WindowsInput;
using WindowsInput.Native;

namespace csv_to_input
{
    internal class AutoInput
    {
        enum State
        {
            ReadyToStart,
            Waiting,
            Running
        }
        private List<string[]> lines;
        private InputSimulator inputSimulator;
        private readonly string[] actions;
        private readonly int delay;
        private State state;

        public AutoInput(List<string[]> lines, string[] actions, int delay)
        {
            this.lines = lines;
            this.actions = actions;
            this.delay = delay;
        }

        public Action EnterWaitingForStart { get; internal set; }
        public Action EnterWaitingForContinue { get; internal set; }
        public Action<int> EndOfLine { get; internal set; }

        internal void Start()
        {
            inputSimulator = new InputSimulator();
            int recordCount = 0;
            foreach (var line in lines)
            {
                ResetActions(line);
                bool loop = true;
                while (loop)
                {
                    // wait for key presses
                    if (state == State.ReadyToStart && ShortcutStartPressed)
                        state = State.Running;

                    if (state == State.Waiting && ShortcutContinuePressed)
                        state = State.Running;

                    // run
                    if (state == State.Running)
                    {
                        inputSimulator.Keyboard.Sleep(delay);
                        var nextAction = GetNextAction();

                        switch (nextAction)
                        {
                            case "insert":
                                var text = GetNextText();
                                if (!string.IsNullOrEmpty(text))
                                    inputSimulator.Keyboard.TextEntry(text);
                                break;
                            case "tab":
                                inputSimulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
                                break;
                            case "enter":
                                inputSimulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);
                                break;
                            case "wait":
                                state = State.Waiting;
                                EnterWaitingForContinue();
                                break;
                            case "end":
                            default:
                                recordCount++;
                                EndOfLine(recordCount);
                                loop = false;
                                break;
                        }
                    }
                }
            }
        }

        private void ResetActions(string[] line)
        {
            state = State.ReadyToStart;
            EnterWaitingForStart();
            nextTextIndex = 0;
            nextActionIndex = 0;
            currentLine = line;
        }

        private int nextTextIndex;
        private string[] currentLine;
        private string GetNextText()
        {
            var text = currentLine[nextTextIndex];
            nextTextIndex++;
            return text;
        }

        private int nextActionIndex;

        private string GetNextAction()
        {
            if (!ActionLeft())
            {
                return "exit";
            }

            var action = actions[nextActionIndex];

            if (action == "insert" && !TextLeft())
            {
                return "exit";
            }

            nextActionIndex++;
            return action;
        }

        private bool ActionLeft() => actions.Length > nextActionIndex;

        private bool TextLeft() => currentLine.Length > nextTextIndex;

        private bool ShortcutStartPressed => ShortCutPressed(VirtualKeyCode.CONTROL, VirtualKeyCode.INSERT);
        private bool ShortcutContinuePressed => ShortCutPressed(VirtualKeyCode.INSERT);
        private bool ShortCutPressed(params VirtualKeyCode[] keys)
        {            
            var allKeysDown = keys.All(key => inputSimulator.InputDeviceState.IsKeyDown(key));
            if (allKeysDown) {
                var loop = true;
                while(loop) {
                    var allKeysUp = keys.All(key => !inputSimulator.InputDeviceState.IsKeyDown(key));
                    if(allKeysUp) {
                        loop = false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    
}
