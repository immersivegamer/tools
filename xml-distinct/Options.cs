using CommandLine;

namespace xml_distinct {

    public class Options {
        [Option('i', "inputFile", Required = true)]
        public string inputFile { get; set; }

        [Option('o', "outputFile", Required = true)]
        public string outputFile { get; set; }

        [Option('e', "existingFile")]
        public string existingFile { get; set; }

        [Option('s', "stream")]
        public bool streamInput { get; set; }
    }
}