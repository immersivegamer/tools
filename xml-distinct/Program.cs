﻿using System;
using CommandLine;
using System.IO;
using System.Xml.Linq;

namespace xml_distinct
{
    class Program
    {
        static void Main(string[] args)
        {
            #if DEBUG
                Run(new Options { inputFile = "./example.xml", outputFile = "./example-out.xml", existingFile = "./example-out.xml", streamInput = true});
            #else
                CommandLine.Parser.Default.ParseArguments<Options>(args).WithParsed(Run);
            #endif
        }

        static void Run(Options options) {

            XElement distinctDocument = null;
            if(options.streamInput) 
            {
                // using XmlReader stream in second level and merge second level into XElement
                distinctDocument = new StreamDistinctXml(options.inputFile).Run().GetFinal();
            }
            else 
            {
                // load full xml into memory into XElement directly
                XElement document = XElement.Parse(File.ReadAllText(options.inputFile));
                distinctDocument = new XElement(document.Name);
                Replicate(document, distinctDocument);
            }


            if (!string.IsNullOrEmpty(options.existingFile) && File.Exists(options.existingFile))
            {
                // if existing document then merge into it
                var document = distinctDocument;
                distinctDocument = XElement.Parse(File.ReadAllText(options.existingFile));
                Replicate(document, distinctDocument);
            }

            distinctDocument.Save(options.outputFile);
        }

        // todo: refactor into separate class
        internal static void Replicate(XElement originalElement, XElement distinctElement) {
            // copy attributes
            foreach(var attribute in originalElement.Attributes()) {
                var distinctAttribute = distinctElement.Attribute(attribute.Name);
                if (null == distinctAttribute) {
                    distinctElement.Add(attribute);
                    continue;
                }

                if (string.IsNullOrEmpty(distinctAttribute.Value) && !string.IsNullOrEmpty(attribute.Value)) {
                    distinctAttribute.Value = attribute.Value;
                }
            }

            // copy value (if no children and value is empty)
            if (!originalElement.HasElements) {
                if (string.IsNullOrEmpty(distinctElement.Value) && !string.IsNullOrEmpty(originalElement.Value)) {
                    distinctElement.Value = originalElement.Value;
                }
            }

            // copy elements
            foreach(var child in originalElement.Elements()) {
                var distinctChild = distinctElement.Element(child.Name);
                if (null == distinctChild) {
                    distinctChild = new XElement(child.Name);
                    distinctElement.Add(distinctChild);
                }
                Replicate(child, distinctChild);
            }
        }
    }
}
