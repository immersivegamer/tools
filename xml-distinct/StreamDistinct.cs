using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace xml_distinct
{
    public class StreamDistinctXml {
        private readonly string xmlUri;
        private string rootName;
        private Dictionary<string, XElement> secondLevelNodes = new Dictionary<string, XElement>();
        public StreamDistinctXml(string uri) {
            this.xmlUri = uri;
        }

        public XElement GetFinal() {
            return new XElement(rootName,
                secondLevelNodes.Values.ToArray()
            );
        }
        
        public StreamDistinctXml Run() {
            foreach(var element in XLooper()) {
                var name = element.Name.LocalName;
                if(!secondLevelNodes.ContainsKey(name))
                {
                    var doc = new XElement(name);
                    Program.Replicate(element, doc);
                    secondLevelNodes.Add(name, doc);
                    continue;
                }

                var distinct = secondLevelNodes[name];
                
                Program.Replicate(element, distinct);
            }
            return this;
        }

        public IEnumerable<XElement> XLooper() {
            // var readFirst = false;
            using (XmlReader reader = XmlReader.Create(xmlUri))
            {
                reader.MoveToContent();
                rootName = reader.Name;
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            // if(!readFirst) {
                            //     rootName = reader.Name;
                            //     readFirst = true;
                            //     continue;
                            // }

                            var element = XElement.ReadFrom(reader) as XElement; // get the sub tree for this node under the root
                            yield return element;
                            break;
                    }
                }
                reader.Close();
            }
        }
    }
}