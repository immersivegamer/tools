namespace csv_to_xml
{
    internal class ArgumentState
    {
        enum arg_state
        {
            none,
            input,
            output,
            config,
            config_sans_output
        }

        private arg_state state;

        internal bool None => state == arg_state.none;
        internal bool HasConfig => state == arg_state.config || state == arg_state.config_sans_output;
        internal bool CanWriteFile => state == arg_state.config || state == arg_state.output;

        public ArgumentState(string[] args)
        {
            state = GetArgumentState(args);
        }

        private arg_state GetArgumentState(string[] args)
        {
            var state = arg_state.none;

            switch (args.Length)
            {
                case 0:
                    System.Console.WriteLine("No args provided, see readme.");
                    break;
                case 1:
                    state = arg_state.input;
                    break;
                case 2:
                    state = arg_state.output;
                    break;
                case 3:
                default:
                    state = arg_state.config;
                    if (args[1] == "out")
                        state = arg_state.config_sans_output;
                    break;
            }

            return state;
        }
    }
}