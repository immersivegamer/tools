## Details

### About

Tool to convert a flat file CSV to a nested XML. Each CSV column can be mapped to a node by specifying a path.

### Language

C#, .Net Core (Console) Project

### Dependencies

| Name            | Link                                   | About                         |
|-----------------|----------------------------------------|-------------------------------|
| CsvHelper       | https://joshclose.github.io/CsvHelper/ | Used to read the csv file.    |
| Netwonsoft.Json | https://www.newtonsoft.com/json        | Used to read config.json file |

## Usage

Build project. Note, CSV is streamed when read but XML is fully built in memory and then written to output or file.

```powershell
# navigate to project root
dotnet build -c release

# default output to: ./bin/release/netcoreapp2.2/
```

Run the program. You will need to supply the date range.

```powershell
# basic usage, assumes ./config.json in execution folder
dotnet csv-to-xml.dll ./input.csv ./output.xml

# specifying config to use
dotnet csv-to-xml.dll ./input.csv ./output.xml ./my-config.json

# leave out output path or put 'out' to write to stdout
dotnet csv-to-xml.dll ./input.csv
dotnet csv-to-xml.dll ./input.csv out ./my-config.json
```

## Example inptut and output

Config JSON:

```json
{
    "root":"places",
    "item":"place",
    "mapping": {
        "cola": "id",
        "colb": "address/street",
        "colc": "address/city"
    }
}
```

Input CSV:

| cola     | colb                 | colc    |
|----------|----------------------|---------|
| 19500101 | 802 Village Green Ln | Madison |
| 19500102 | 4638 Hayes Rd        | Madison |

Output XML:

```xml
<places>
  <place>
     <id>19500101</id>
     <address>
       <street>802 Village Green Ln</street>
       <city>Madison</city>
     </address>
  </place>
  <place>
     <id>19500102</id>
     <address>
       <street>4638 Hayes Rd</street>
       <city>Madison</city>
     </address>
  </place>
</places>
```