using System;
using System.Linq;
using System.Xml.Linq;

namespace csv_to_xml
{
    internal class XmlBuilder
    {
        private string rootName;
        private string itemName;
        private XElement document;
        private XElement activeItem;

        public XmlBuilder(string root, string item)
        {
            this.rootName = root;
            this.itemName = item;

            document = new XElement(root);
        }

        internal void NewItem()
        {
            activeItem = new XElement(itemName);
            document.Add(activeItem);
        }

        internal void AddToCurrent(string path, string value)
        {
            string[] names = path.Split("/").Where(x => !string.IsNullOrEmpty(x)).ToArray();

            // navigate down the xml nodes, if missing then add node
            XElement node = activeItem;
            foreach(var name in names)
            {
                if(node.HasElements && node.Elements(name).Any())
                {
                    node = node.Element(name);
                } 
                else 
                {
                    var newNode = new XElement(name);
                    node.Add(newNode);
                    node = newNode;
                }
            }

            // last node is target node, set value
            node.Value = value;
        }

        internal string DocumentAsString()
        {
            return document.ToString();
        }

        
    }
}