using System.Collections.Generic;
using CsvHelper;

namespace csv_to_xml
{
    class Converter
    {
        private readonly Config c;

        public Converter(Config c)
        {
            this.c = c;
        }

        public string Read(string filePath)
        {
            var builder = new XmlBuilder(c.root, c.item);

            using (var fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open))
            using (var textReader = new System.IO.StreamReader(fileStream))
            {
                var parser = new CsvParser(textReader);
                int[] activeColumns;
                string[] activeMaps;
                

                ParseHeaderRow(out activeColumns, out activeMaps, parser.Read());

                int columnsMapped = activeColumns.Length;
                
                while (true)
                {
                    string[] row = parser.Read();

                    if (row == null)
                    {
                        break;
                    }
                    
                    builder.NewItem();
                    for(int i = 0; i < columnsMapped; i++)
                    {
                        builder.AddToCurrent(activeMaps[i], row[i]);
                    }
                }
            }

            return builder.DocumentAsString();
        }

        private void ParseHeaderRow(out int[] activeColumns, out string[] activeMaps, string[] row)
        {
            var activeColumnsList = new List<int>();
            var activeMapsList = new List<string>();
            for (int i = 0; i < row.Length; i++)
            {
                if (c.mapping.ContainsKey(row[i]))
                {
                    activeColumnsList.Add(i);
                    activeMapsList.Add(c.mapping[row[i]]);
                }
            }
            activeColumns = activeColumnsList.ToArray();
            activeMaps = activeMapsList.ToArray();
        }
    }
}
