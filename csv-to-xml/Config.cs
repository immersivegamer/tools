using System.Collections.Generic;
using Newtonsoft.Json;

public class Config
{
    public string root { get; set; }
    public string item { get; set; }
    public Dictionary<string, string> mapping { get; set; }

    public static Config LoadFromFile(string filePath)
    {
        var configData = System.IO.File.ReadAllText(filePath);
        return JsonConvert.DeserializeObject<Config>(configData);
    }
}