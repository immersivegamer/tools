﻿using System;

namespace csv_to_xml
{
    class Program
    {
        static int Main(string[] args)
        {
            var state = new ArgumentState(args);

            if (state.None)
                return 1;

            var configPath = state.HasConfig ? args[2] : "./config.json";
            var config = Config.LoadFromFile(configPath);
            var xmlAsString = new Converter(config).Read(args[0]);

            if (state.CanWriteFile)
            {
                System.IO.File.WriteAllText(args[1], xmlAsString);
            }
            else
            {
                System.Console.WriteLine(xmlAsString);
            }

            return 0;
        }

    }
}
