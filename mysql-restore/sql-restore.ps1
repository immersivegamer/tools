param(
    [string]$file,
    [string]$configPath = 'config.yaml'
)

# load config
$config = ConvertFrom-Yaml -Yaml (Get-Content $configPath -Raw)

. "$PSScriptRoot/sql-restore-functions.ps1"

# backup
if ($config.options.backupExisting) {
    New-Item -ItemType Directory -Path $config.options.backupPath -Force -ErrorAction SilentlyContinue
    $backupFullPath = Join-Path $config.options.backupPath ($config.connect.database + '-' + (Get-date -format 'yyyyMMdd-hhmmss') + '.sql')
    Write-Host "Backup database ..."
    & $config.options.mysqldump (New-ConnectArgs $true) | Set-Content $backupFullPath
}

# delete
if ($config.options.dropIfExists) {
    Write-Warning "Drop database ..."
    & $config.options.mysql ((New-ConnectArgs) + @('-e', "DROP DATABASE IF EXISTS $($config.connect.database);"))
}

# before
Write-Host "Executing before scripts ..."
foreach($script in $config.before) {
    Use-Script $script $config.options.beforeScriptPath $false
}

# restore
if ($config.options.createIfNotExists) {
    Write-Host "Create database if does not exist ..."
    & $config.options.mysql ((New-ConnectArgs) + @('-e', "CREATE DATABASE IF NOT EXISTS $($config.connect.database);")) 
}

Write-Host "Restore database ..."
$fileWacked = $file.Replace('\', '/')
& $config.options.mysql ((New-ConnectArgs $true) + @('-e', "source $fileWacked;"))

# after
Write-Host "Execute after scripts ..."
foreach($script in $config.after) {
    Use-Script $script $config.options.afterScriptPath $true
}

# cleanup
if ($config.options.clearTempFiles) {
    Write-Host "Cleaning up temp files ..."
    foreach($p in @($config.options.afterScriptPath, $config.options.beforeScriptPath)) {
        foreach($f in Get-ChildItem -Path $p -Recurse -Filter '*.temp') {
            Remove-Item $f.FullName -Force -ErrorAction SilentlyContinue
        }
    }
}