update employees
set lastName = concat(employeeNumber, 'last'),
    firstName = concat(employeeNumber, 'first'),
    email = concat(employeeNumber, '@classicmodelcars.com');