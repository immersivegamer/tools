update customers
set customerName = concat(customerNumber, 'FullName'), 
    contactLastName = concat(customerNumber, 'C_Last'),
    contactFirstName = concat(customerNumber, 'C_First'),
    phone = customerNumber * 1000,
    addressLine1 = concat(customerNumber, ' st.'),
    addressLine2 = null;