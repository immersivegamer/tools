
function New-ConnectArgs([boolean]$withDatabase=$false) {
    $argArray = @(('-h'+$config.connect.host), ('-u'+$config.connect.user))
    if ($config.connect.password) {
        $password = '-p'+$config.connect.password
        if($config.connect.password -eq 'ask') {
            $password = '-p'
        }
        $argArray += $password
    }

    if($withDatabase) {
        $argArray += $config.connect.database
    }
    
    return $argArray
}

function New-Script($script, $directory) {
    $fullPath = Join-Path $directory $script.file
    $replaces = @($script.replaces) + @($config.globalReplaces)
    if ($replaces.Count -gt 0) {
        $fullPathTemp = $fullPath + '.temp'
        Set-Content -Value "" -Path $fullPathTemp
        foreach($line in (Get-Content $fullPath)) {
            foreach($r in $replaces) {
                $line = $line -replace $r.from,$r.to 
            }
            $line | Add-Content $fullPathTemp
        }
        $fullPath = $fullPathTemp
    }
    return $fullPath
}

function Use-Script($script, $directory, [boolean]$useDatabase=$false) {
    $argArray = New-ConnectArgs $useDatabase
    $fileName = New-Script $script $directory
    $actionText = $script.file
    if($script.description) {
        $actionText = $script.description
    }

    Write-Host "Run: " -NoNewline
    Write-Host $actionText
    Get-Content $fileName | & $config.options.mysql $argArray
}